package it.unibo.pacman.controller.entities;

public interface PacManController {
    void move();
    void eat();
    void updateView();
}
