package it.unibo.pacman.controller.entities;

import it.unibo.pacman.model.collision.CollisionController;
import it.unibo.pacman.model.entities.LivingEntity;
import it.unibo.pacman.view.entities.LivingEntityView;

public abstract class AbstractLivingEntityController<X extends LivingEntity> implements LivingEntityController {

    private X entity;
    private LivingEntityView entityView;
    private CollisionController cc;
    public AbstractLivingEntityController(final X entity, final LivingEntityView entityView, final CollisionController cc) {
        this.entity = entity;
        this.entityView = entityView;
        this.cc = cc;
    }

    @Override
    public void updateView() {
        this.entityView.update(this.entity.getDirection(), this.entity.getStatus(), this.entity.getPosition());
    }

    @Override
    public abstract void resolveCollision();

    @Override
    public void move() {
        this.entity.move();
    }

    public X getEntity() {
        return entity;
    }
    public LivingEntityView getEntityView() {
        return entityView;
    }
    public CollisionController getCollisionController() {
        return cc;
    }
}
