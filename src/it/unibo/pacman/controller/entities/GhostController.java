package it.unibo.pacman.controller.entities;

import it.unibo.pacman.model.collision.CollisionController;
import it.unibo.pacman.model.entities.LivingEntity;
import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;
import it.unibo.pacman.view.entities.LivingEntityView;

public class GhostController extends AbstractLivingEntityController<LivingEntity> {

    private static final Pair<Integer, Integer> STANDARDSPAWN = new Pair<>(1*32, 1*32);
    private static final Direction STANDARDDIR = Direction.UP;

    public GhostController(final LivingEntity entity, final LivingEntityView entityView, final CollisionController cc) {
        super(entity, entityView, cc);
    }

    @Override
    public void resolveCollision() {
        if (this.getCollisionController().touchedAWall(this.getEntity())) {
            this.getEntity().setDirection(Direction.getOpposite(this.getEntity().getDirection()));
            this.move();
            this.getEntity().setDirection(this.generateDirection());
        }
        if (this.getCollisionController().touchedAPacMan(this.getEntity())) {
            if (this.getEntity().getStatus().equals(Status.CHASED)) {
                this.getEntity().setPosition(STANDARDSPAWN);
                this.getEntity().setDirection(STANDARDDIR);
            }
        }
    }
    private Direction generateDirection() {
        Direction generateDir;
        do {
            generateDir = Direction.getRandomDirection();
        } while (this.getEntity().getDirection() == generateDir);
        return generateDir;
    }

}
