package it.unibo.pacman.controller.entities;

import java.util.Set;

import it.unibo.pacman.model.collision.CollisionController;
import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.entities.MortalEntity;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.view.entities.LivingEntityView;

public class PacManControllerImpl implements PacManController{

    private LivingEntityView view;
    private MortalEntity entity;
    private CollisionController cc;
    private Set<IAController> ghosts; 
    private Set<InanimateEntityController> food;
    //Scoreboard score
    public PacManControllerImpl(final LivingEntityView view, final MortalEntity entity, final CollisionController cc,
            final Set<IAController> ghosts, final Set<InanimateEntityController> food) {
        this.view = view;
        this.entity = entity;
        this.cc = cc;
        this.ghosts = ghosts;
        this.food = food;
        //score
    }

    @Override
    public void move() {
        if (cc.checkCollision(entity.getBoundsAt(entity.nextPosition()), EntityType.WALL).isEmpty()) {
            entity.setPosition(entity.nextPosition());
        }
    }

    @Override
    public void eat() {
        
        food.stream().forEach(e -> {
           Set<Entity> eaten = cc.checkCollision(entity.getBounds(), e.getEntityType());
           //punteggio
           e.remove(eaten);
        });
        Set<Entity> ghostTouched = cc.checkCollision(entity.getBounds(), EntityType.BLINKY);
    }

    @Override
    public void updateView() {
       view.update(entity.getDirection(), entity.getStatus(), entity.getPosition());
        
    }

}
