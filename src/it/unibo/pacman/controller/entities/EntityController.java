package it.unibo.pacman.controller.entities;

public interface EntityController {
void updateView();
void resolveCollision();
}
