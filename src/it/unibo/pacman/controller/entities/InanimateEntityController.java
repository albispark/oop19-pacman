package it.unibo.pacman.controller.entities;

import java.util.Set;

import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.utilities.EntityType;

public interface InanimateEntityController {
void add(Entity entityToAdd);
void remove(Set<Entity> entitiesToRemove);
void updateView();
EntityType getEntityType();
}
