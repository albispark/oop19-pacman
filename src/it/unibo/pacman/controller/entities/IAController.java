package it.unibo.pacman.controller.entities;

public interface IAController {
void move();
void fear();
void updateView();
}
