package it.unibo.pacman.controller.entities;

import java.util.Set;
import java.util.stream.Collectors;

import it.unibo.pacman.model.collision.CollisionController;
import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.view.entities.UnlivingEntityView;

public class PillsController implements EntityController {

    private Set<Entity> pillSet;
    private CollisionController cc;
    private UnlivingEntityView uev;

    public PillsController(final Set<Entity> pillSet, final CollisionController cc, final UnlivingEntityView uev) {
        super();
        this.pillSet = pillSet;
        this.cc = cc;
        this.uev = uev;
    }

    @Override
    public final void updateView() {
        this.uev.update(this.pillSet.stream()
                                    .map(p -> p.getPosition())
                                    .collect(Collectors.toSet()));
    }

    @Override
    public final void resolveCollision() {
        this.pillSet.removeAll(this.pillSet.stream()
                                      .filter(p -> cc.touchedAPacMan(p))
                                      .collect(Collectors.toSet()));
    }

    public Set<Entity> getPillSet() {
        return pillSet;
    }
    
    public CollisionController getCc() {
        return cc;
    }
    
    public UnlivingEntityView getUev() {
        return uev;
    }
    
}
