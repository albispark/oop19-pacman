package it.unibo.pacman.controller.entities;

public interface LivingEntityController extends EntityController {
    void move();
}
