package it.unibo.pacman.controller.game;

import java.util.Set;

import it.unibo.pacman.controller.entities.GhostController;
import it.unibo.pacman.controller.entities.PacManController;
import it.unibo.pacman.controller.entities.PillsController;
import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.entities.LivingEntity;
import it.unibo.pacman.model.entities.MortalEntity;

public interface GameController {

    /**
     * Used to start the game.
     */
    void startGame(final String mapName);
    /**
     * Used to notify the end of game.
     */
    void endGame();
    /**
     * Used to know if game is over.
     * @return true if pacman is dead
     */
    boolean isGameOver();
    /**
     * Used to know if the player has won.
     * @return true if the pacman has eaten all the pills
     */
    boolean hasWon();
    /**
     * Used to update any entity in the world.
     */
    void update();
    Set<Entity> getEntitySet();
}
