package it.unibo.pacman.controller.game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.concurrent.TimeUnit;

import it.unibo.pacman.model.utilities.Direction;


/*
 * Used to move the PacMan because it is in QUARANTEE...NIGGAAAAAAA
 */
public class KeyInput implements KeyListener {
    private final GameController gc;
    /**
     * KeyInput.
     * @param gc {@link GameController}
     */
    public KeyInput(final GameController gc) {
        this.gc = gc;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyPressed(final KeyEvent e) {
        switch (e.getKeyCode()) {
        case KeyEvent.VK_W:
        case KeyEvent.VK_UP:
            gc.getPacman().setDirection(Direction.UP);
            break;
        case KeyEvent.VK_S:
        case KeyEvent.VK_DOWN: 
            gc.getPacman().setDirection(Direction.DOWN);
            break;
        case KeyEvent.VK_A:
        case KeyEvent.VK_LEFT: 
            gc.getPacman().setDirection(Direction.LEFT);
            break;
        case KeyEvent.VK_D:
        case KeyEvent.VK_RIGHT:
            gc.getPacman().setDirection(Direction.RIGHT);
            break;
        default:
            break;
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void keyReleased(final KeyEvent e) {
    }

    @Override
    public void keyTyped(final KeyEvent e) {
    }

}
