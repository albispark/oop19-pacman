package it.unibo.pacman.controller.game;

import static it.unibo.pacman.model.utilities.EntityType.GHOST;
import static it.unibo.pacman.model.utilities.EntityType.PACMAN;
import static it.unibo.pacman.model.utilities.EntityType.PILL;
import static it.unibo.pacman.model.utilities.EntityType.POWERPILL;
import static it.unibo.pacman.model.utilities.EntityType.WALL;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.entities.Ghost;
import it.unibo.pacman.model.entities.PacMan;
import it.unibo.pacman.model.entities.Pill;
import it.unibo.pacman.model.entities.Wall;
import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.MapReader;
import it.unibo.pacman.model.utilities.Pair;

public class LoadMap {
    private static final int SPEED = 1;
    private Set<Entity> entities;
    private static final int SCALE = 30;

    public LoadMap(final String mapName) throws IOException {
        this.build(mapName);
    };

    private LoadMap build(final String mapName) throws IOException {
        MapReader.readMap(mapName);
        String current;
        char value;
        entities = new HashSet<>();

        for (int i = 0; i < MapReader.getRows(); i++) {
            current = MapReader.getNext();
            for (int j = 0; j < MapReader.getColumns(); j++) {
                value = current.charAt(j);
                switch (value) {
                case '0':
                    this.entities.add(new Wall(new Pair<>(j * SCALE, i * SCALE), WALL));
                case '1':
                    this.entities.add(new Pill(new Pair<>(j * SCALE, i * SCALE), PILL));
                case '2':
                    this.entities.add(new Pill(new Pair<>(j * SCALE, i * SCALE), POWERPILL));
                case '3':
                    this.entities.add(new PacMan(new Pair<>(j * SCALE, i * SCALE), PACMAN, Direction.LEFT, SPEED));
                case '4':
                    this.entities.add(new Ghost(new Pair<>(j * SCALE, i * SCALE), GHOST, Direction.UP, SPEED));
                case '5':
                    this.entities.add(new Pill(new Pair<>(j * SCALE, i * SCALE), PILL));
                default:
                    break;
                }
            }
        }
        return this;
    }

    public final Set<Entity> getEntities() {
        return entities;
    }
}
