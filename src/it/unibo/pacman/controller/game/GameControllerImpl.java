package it.unibo.pacman.controller.game;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import it.unibo.pacman.model.leaderboard.PlayerScoreImpl;
import it.unibo.pacman.controller.entities.GhostController;
import it.unibo.pacman.controller.entities.PacManController;
import it.unibo.pacman.controller.entities.PillsController;
import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.entities.LivingEntity;
import it.unibo.pacman.model.entities.MortalEntity;
import it.unibo.pacman.model.loop.GameEngine;
import it.unibo.pacman.model.loop.GameEngineImp;
import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.MapReader;
import it.unibo.pacman.view.MainMenuView;
import it.unibo.pacman.view.SinglePlayerView;

public class GameControllerImpl implements GameController {
    private LoadMap loadMap;
    private Set<GhostController> ghostsController; 
    private PillsController pillsController; 
    private PacManController pacmanController;
    private PillsController wallsController;
    private PlayerScoreImpl ps;
    private SinglePlayerView spv;
    private Set<Entity> entities;
    private final MainMenuView mainView;
    private GameEngine engine;
    private String mapName;
    //private final Level lv = new LevelImpl();

    /**
     * Construct an implementation of {@link GameController}.
     * @param menuView {@link MainMenuView}
     */
    public GameControllerImpl(final MainMenuView menuView) {
        this.mainView = menuView;
    }
    /**
     * {@inheritDoc}
     */
    public void startGame(final String mapName) {
        this.mapName = mapName;
        this.ps = new PlayerScoreImpl();
        entities = new HashSet<>();
        try {
            entities = new LoadMap(mapName).getEntities();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.pacmanController = this.loadMap.getPacmanController();
        this.ghostsController = this.loadMap.getGhostsController();
        this.pillsController = this.loadMap.getPillsController();
        this.wallsController = this.loadMap.getWallController();
//        this.pacManView = Collections.singleton(this.loadMap.getPacmanController().getEntityView());
//        this.ghostView = Collections.singleton(this.loadMap.getPacmanController().getEntityView());
//        this.pillViewSet = Collections.singleton(this.pillsController.getUev());
//        this.wallViewSet = Collections.singleton(this.loadMap.getPacmanController().getEntityView());
        //this.ps = new PlayerScoreImpl();
        this.engine = new GameEngineImp(this);
        try {
            this.spv = new SinglePlayerView(new KeyInput(this), MapReader.getRows(), MapReader.getColumns()/*,this.ps,*/);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.engine.startEngine();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void endGame() {
        this.spv.getFrame().setVisible(false);
        //final EndView end = new EndView(mainView, this, spv, ps);
        //end.getFrame().setVisible(true);
    }
    /**
     * 
     * @return true if the hero is dead
     */
    public boolean isGameOver() {
        return this.pacmanController.getEntity().isDead();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasWon() {
        return this.pillsController.getPillSet().isEmpty();
    }
    private void resolveCollision() {
        this.ghostsController.stream().forEach(g -> g.resolveCollision());
        this.pillsController.resolveCollision();
        this.pacmanController.resolveCollision();
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void update() {
        this.spv.getGraphics().fillRect(0, 0, 520, 600);
        resolveCollision();
        this.pacmanController.getEntity().move();
        this.ghostsController.stream().forEach(g -> g.move());
        this.pacmanController.updateView();
        this.pillsController.updateView();
        this.ghostsController.stream().forEach(g -> g.updateView());
        this.spv.render(Collections.singleton(this.pillsController.getUev()));
        this.spv.render(Collections.singleton(this.wallsController.getUev()));
        this.spv.render(Collections.singleton(this.pacmanController.getEntityView()));
        this.spv.render(this.ghostsController.stream().map(g -> g.getEntityView()).collect(Collectors.toSet()));
        this.spv.getBs().show();
        this.spv.getCanvas().requestFocus();
        this.spv.getGraphics().dispose();
    }

    
    
    public final Set<Entity> getEntitySet(){
        return this.entities;
    }
    
}
