package it.unibo.pacman.controller;

import it.unibo.pacman.view.MainMenuView;

/**
 * Start the application.
 *
 */
public final class Launcher {

    private static final String TITLE = "PACMAN HERO";
    /**
     * Empty constructor.
     */
    private Launcher() {
    }

    /**
     * Main method.
     * @param args
     */
    public static void main(final String[] args) {
        new MainMenuView(TITLE).load();
    }

}
