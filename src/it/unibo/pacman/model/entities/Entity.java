package it.unibo.pacman.model.entities;

import java.awt.Rectangle;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;

public interface Entity {
    Rectangle getBounds();
    Pair<Integer, Integer> getPosition();
    EntityType getType();
}
