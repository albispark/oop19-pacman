package it.unibo.pacman.model.entities;

import java.awt.Rectangle;
import java.util.Random;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.GhostType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;


public class Ghost extends AbstractLivingEntity {
    private int speed;

    public Ghost(final Pair<Integer, Integer> position, final EntityType type, final Direction firstDir, final int speed) {
        super(firstDir, Status.CHASING, position, type);
        this.speed = speed;
    }
    @Override
    public final void move() {
       switch (this.getDirection()) {
       case DOWN:
           this.setPosition(new Pair<>(this.getPosition().getX(), this.getPosition().getY() + speed));
           break;
       case LEFT:
           this.setPosition(new Pair<>(this.getPosition().getX() - speed, this.getPosition().getY()));
           break;
       case RIGHT:
           this.setPosition(new Pair<>(this.getPosition().getX() + speed, this.getPosition().getY()));
           break;
       case UP:
           this.setPosition(new Pair<>(this.getPosition().getX(), this.getPosition().getY() - speed));
           break;
       default:
           break;
       }
    }
    private void stepBack() {
        switch (this.getDirection()) {
        case DOWN:
            this.setPosition(new Pair<>(this.getPosition().getX(), this.getPosition().getY() - speed));
            break;
        case LEFT:
            this.setPosition(new Pair<>(this.getPosition().getX() + speed, this.getPosition().getY()));
            break;
        case RIGHT:
            this.setPosition(new Pair<>(this.getPosition().getX() - speed, this.getPosition().getY()));
            break;
        case UP:
            this.setPosition(new Pair<>(this.getPosition().getX(), this.getPosition().getY() + speed));
            break;
        default:
            break;
        }
    }

    public final Rectangle getBounds() {
        return this.getBoundsAt(this.getPosition());
    }
    @Override
    public Rectangle getBoundsAt(final Pair<Integer, Integer> position) {
        return new Rectangle(position.getX(), position.getY(), 27, 27);
    }
    @Override
    public Pair<Integer, Integer> nextPosition() {
        // TODO Auto-generated method stub
        return null;
    }
}
