package it.unibo.pacman.model.entities;

import java.awt.Rectangle;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;

public abstract class AbstractEntity implements Entity{
    private Pair<Integer, Integer> position;
    private EntityType type;
    public AbstractEntity(final Pair<Integer, Integer> position, final EntityType type) {
        this.position = position;
        this.type = type;
    }
    public Pair<Integer, Integer> getPosition() {
        return position;
    }
    public void setPosition (final Pair<Integer, Integer> position) {
        this.position = position;
    }
    public EntityType getType() {
        return type;
    }
    public void setType(final EntityType type) {
        this.type = type;
    }
    public abstract Rectangle getBounds();
}
