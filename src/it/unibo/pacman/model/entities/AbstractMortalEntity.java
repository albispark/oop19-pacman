package it.unibo.pacman.model.entities;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;

public abstract class AbstractMortalEntity  extends AbstractLivingEntity implements MortalEntity {
    private boolean death;
    private int life;
    public AbstractMortalEntity(final Direction actualDir, final Status status, final Pair<Integer, Integer> position, final EntityType type) {
        super(actualDir, status, position, type);
        this.death = false;
    }
    public boolean isDead() {
        return death;
    }
    public void setDeath(boolean death) {
        this.death = death;
    }
    public int getLife() {
        return life;
    }
    public void setLife(int life) {
        this.life = life;
    }
    public abstract void loseOneLife();
}
