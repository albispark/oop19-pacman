package it.unibo.pacman.model.entities;

import java.awt.Rectangle;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;

public class Wall extends AbstractEntity {

    public Wall(final Pair<Integer, Integer> position, final EntityType type) {
        super(position, type);
    }

    @Override
    public final Rectangle getBounds() {
        // TODO Auto-generated method stub
        return new Rectangle(this.getPosition().getX(),this.getPosition().getY(), 30, 30);
    }

}
