package it.unibo.pacman.model.entities;

public interface MortalEntity extends LivingEntity {
boolean isDead();
void loseOneLife();
}
