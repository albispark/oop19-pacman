package it.unibo.pacman.model.entities;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;

public abstract class AbstractLivingEntity extends AbstractEntity implements LivingEntity {
    private Direction actualDir;
    private Status status;
    public AbstractLivingEntity(final Direction actualDir, final Status status, final Pair<Integer, Integer> position, final EntityType type) {
        super(position, type);
        this.actualDir = actualDir;
        this.status = status;
    }
    public final Direction getDirection() {
        return actualDir;
    }
    public final void setDirection(final Direction actualDir) {
        this.actualDir = actualDir;
    }
    public final Status getStatus() {
        return status;
    }
    public final void setStatus(final Status status) {
        this.status = status;
    }
    public abstract void move();
}
