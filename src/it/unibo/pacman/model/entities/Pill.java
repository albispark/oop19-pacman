package it.unibo.pacman.model.entities;

import java.awt.Rectangle;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;

public class Pill extends AbstractEntity {
    public Pill(final Pair<Integer, Integer> position, final EntityType type) {
        super(position, type);
    }

    @Override
    public final Rectangle getBounds() {
        return new Rectangle(this.getPosition().getX(),this.getPosition().getY(), 30, 30);
    }
}
