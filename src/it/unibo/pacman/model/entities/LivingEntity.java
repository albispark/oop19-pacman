package it.unibo.pacman.model.entities;

import java.awt.Rectangle;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;

public interface LivingEntity extends Entity {
void setPosition(Pair<Integer, Integer> newposition);
Direction getDirection();
void setDirection(Direction direction);
Status getStatus();
void setStatus(Status newStatus);
void move();
Rectangle getBoundsAt(Pair<Integer, Integer> position);
Pair<Integer, Integer> nextPosition();
}
