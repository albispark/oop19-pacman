package it.unibo.pacman.model.leaderboard;

import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.entities.powerups.AbstractPowerupEntity;
import it.unibo.pacman.model.utilities.EntityType;

public enum Scoring {
    /**
     * bonus's score.
     */
    PILL(50),
    /**
     * malus's score.
     */
    POWERPILL(200),
    /**
     * monster killed's score.
     */
    GHOST(500);

private int value;
/**
 * @param value 
 */
Scoring(final int value) {
    this.value = value;
}
/**
 * Get value of entity.
 * @return value
 */
private int getValue() {
    return value;
}
/**
 * Get the scoring of the given entity.
 * @param entity 
 * @return scoring 
 */
public static int getScoring(final Entity entity) {
    if (entity.getType() == EntityType.POWERPILL) {
        final AbstractPowerupEntity powerUp = (AbstractPowerupEntity) entity;
        return powerUp.isBonus() ? PILL.getValue() : POWERPILL.getValue();
    } else if (entity.getType().equals(EntityType.MONSTER)) {
        return GHOST.getValue();
    }
    return 0;
}
}
