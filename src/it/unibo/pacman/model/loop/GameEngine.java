package it.unibo.pacman.model.loop;

/**
 * 
 * This interface represents the Game Engine. 
 */
public interface GameEngine {
    /**
     * Starts the Game Loop.
     */
    void startEngine();
    /**
     * Stops the Game.
     */
    void stopEngine();
}

