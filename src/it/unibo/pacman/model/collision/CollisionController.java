package it.unibo.pacman.model.collision;

import java.awt.Rectangle;
import java.util.Set;

import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.utilities.EntityType;

public interface CollisionController {
    Set<Entity> checkCollision(Rectangle collider, EntityType type);
}
