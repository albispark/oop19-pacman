package it.unibo.pacman.model.collision;


import java.awt.Rectangle;
import java.util.Set;
import java.util.stream.Collectors;

import it.unibo.pacman.controller.game.GameController;
import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.utilities.EntityType;


public class CollisionControllerImpl implements CollisionController {

    private GameController gc;

    public CollisionControllerImpl(final GameController gc) {
        super();
        this.gc = gc;
    }
    @Override
    public final Set<Entity> checkCollision(final Rectangle collider, final EntityType type) {
        return gc.getEntitySet().stream()
                                .filter(e -> e.getType().equals(type))
                                .filter(e -> e.getBounds().intersects(collider))
                                .collect(Collectors.toSet());
    }
}
