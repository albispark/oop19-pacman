package it.unibo.pacman.model.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public enum GhostType {
    /**
     * Represents an enemy, a phantom.
     *
     */
    BLINKY('1'),
    /**
     * Represents the pink phantom.
     *
     */
    PINKY('2'),
    /**
     * Represents the light blue phantom.
     *
     */
    INKY('3'),
    /**
     * Represents the orange phantom.
     *
     */
    CLYDE('4');
    private final char value;

    GhostType(final char value) {
        this.value = value;
    }

    /**
     * 
     * @return the value of the Enum
     */
    public char getValue() {
        return this.value;
    }
    public String toString() {
        switch (this) {
        case BLINKY:
            return "Blinky";
        case PINKY:
            return "Pinky";
        case INKY:
            return "Inky";
        case CLYDE:
            return "Clyde";
        default:
            return "";
        }
    }
    public static GhostType next() {
       List<GhostType> list = new ArrayList<GhostType>(Arrays.asList(GhostType.values()));
       Iterator<GhostType> iterator = list.iterator();
       if (iterator.hasNext()) {
           return iterator.next();
       } else {
        throw new NoSuchElementException();
    }
    }

}
