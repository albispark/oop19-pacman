package it.unibo.pacman.model.utilities;

public enum Status {
    CHASING,
    CHASED,
    DEAD,
    CHASED_END;
}
