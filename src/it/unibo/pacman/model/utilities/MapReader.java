package it.unibo.pacman.model.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public final class MapReader {
    private static int rows;
    private static int columns;
    private static BufferedReader content;

    private MapReader() {
    }

    public static void readMap(final String mapName) throws IOException {
        String path = "res" + System.getProperty("file.separator") + "Map" + System.getProperty("file.separator") + mapName;
        FileReader reader = new FileReader(path);
        content = new BufferedReader(reader);
        columns = Integer.parseInt(getNext());
        rows = Integer.parseInt(getNext());
    }

//    /**
//     * @param mapName
//     * @throws IOException 
//     */
//    public static void readMap(final String mapName) throws IOException {
//        String path = "/Users/nikolas/Desktop/PROGETTO/oop19-pacman/res/Map";
//        scanner = new Scanner(new File(path + "/" + mapName));
//        System.out.println("SCANNER: " + scanner.nextInt());
////        rows = dis.readInt();
////        columns = dis.readInt();
//    }

    public static int getRows() throws IOException {
        return rows;
    }

    public static int getColumns() throws IOException {
        return columns;
    }

    public static String getNext() throws IOException {
        return content.readLine();
    }
}
