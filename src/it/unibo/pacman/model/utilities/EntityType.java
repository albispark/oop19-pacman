package it.unibo.pacman.model.utilities;

/**
 * Represents the entities in the game.
 *
 */
public enum EntityType {

    /**
     * Represents a wall.
     *
     */
    WALL('0'),
    /**
     * Represents a pill.
     *
     */
    PILL('1'),
    /**
     * Represents a power pill.
     *
     */
    POWERPILL('2'),
    /**
     * Represents the main character Pacman.
     *
     */
    PACMAN('3'),
    /**
     * Represents an enemy, a phantom.
     *
     */
    BLINKY('4'),
    /**
     * Represents the pink phantom.
     *
     */
    PINKY('5'),
    /**
     * Represents the light blue phantom.
     *
     */
    INKY('6'),
    /**
     * Represents the orange phantom.
     *
     */
    CLYDE('7'),
    /**
     * Represents the empty space.
     * 
     */
    EMPTY('8');

    private final char value;

    EntityType(final char value) {
        this.value = value;
    }

    /**
     * 
     * @return the value of the Enum
     */
    public char getValue() {
        return this.value;
    }
    public String toString() {
        switch (this) {
        case WALL:
            return "Wall";
        case PILL:
            return "Pill";
        case POWERPILL:
            return "PowerPill";
        case PACMAN:
            return "Pacman";
        case BLINKY:
            return "Blinky";
        case PINKY:
            return "Pinky";
        case INKY:
            return "Inky";
        case CLYDE:
            return "Clyde";
        case EMPTY:
            return "Empty";
        default:
            return "";
        }
    }
}
