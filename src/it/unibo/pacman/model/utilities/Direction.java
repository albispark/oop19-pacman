package it.unibo.pacman.model.utilities;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public enum Direction {

  /**
   * Used to set the direction to LEFT.
   */

    LEFT,
    /**
     * Used to set the direction to RIGHT.
     */
    RIGHT,
    /**
     * Used to set the direction to UP.
     */
    UP,
    /**
     * Used to set the direction to DOWN.
     */
    DOWN,
    /**
     * Used to STOP the movement.
     */
    STOP;

    public List<Direction> getDirectionList() {
        return Arrays.asList(Direction.values());
    }

    private static final Direction[] VALUES = values();

    public static Direction getRandomDirection() {
        return VALUES[new Random().nextInt(VALUES.length-1)];
    }
    
    public static Direction getOpposite(final Direction dir) {
        switch (dir) {
        case DOWN:
            return UP;
        case LEFT:
            return RIGHT;
        case RIGHT:
            return LEFT;
        case UP:
            return DOWN;
        default:
            return UP;
        }
    }
}
