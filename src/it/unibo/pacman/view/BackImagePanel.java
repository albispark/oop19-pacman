package it.unibo.pacman.view;

import java.awt.Graphics;
import java.awt.LayoutManager;

import javax.swing.JPanel;

import it.unibo.pacman.view.utilities.BufferedImageLoader;

public class BackImagePanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String path;

    public BackImagePanel(final LayoutManager lmg, final String path) {
        super(lmg);
        this.path = path;
    }

    @Override
    protected final void paintComponent(final Graphics g) {

      super.paintComponent(g);
          g.drawImage(BufferedImageLoader.loadImage(path), 0, 0, null);
  }

}
