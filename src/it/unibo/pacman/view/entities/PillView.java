package it.unibo.pacman.view.entities;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Random;
import java.util.Set;

import it.unibo.pacman.model.entities.Entity;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.view.utilities.BufferedImageLoader;

public class PillView implements UnlivingEntityView {
    private EntityType type;
    private Set<Pair<Integer, Integer>> setPosition;
    private Image image;

    public PillView(final EntityType type, final Set<Pair<Integer, Integer>> set) {
        this.type = type;
        this.setPosition = set;
        this.image = BufferedImageLoader.loadImage("res/Map/yellowBlock.png");
    }

    @Override
    public final EntityType getType() {
    return this.type;
    }

    @Override
    public final void render(final Graphics g) {
        setPosition.stream()
                   .forEach(p -> g.drawImage(image, p.getX(), p.getY(),
                                           30, 30, null));
    }

    @Override
    public final void update(final Set<Pair<Integer, Integer>> setPosition) {
        this.setPosition = setPosition;
    }

}
