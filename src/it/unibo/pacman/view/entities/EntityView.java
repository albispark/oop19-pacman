package it.unibo.pacman.view.entities;

import java.awt.Graphics;

import it.unibo.pacman.model.utilities.EntityType;

public interface EntityView {
    EntityType getType();
    void render(Graphics g);
}
