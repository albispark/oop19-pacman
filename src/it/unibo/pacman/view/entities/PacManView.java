package it.unibo.pacman.view.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;
import it.unibo.pacman.view.utilities.BufferedImageLoader;

public class PacManView extends AbstractLivingEntityView {

    public PacManView(final EntityType type, final Pair<Integer, Integer> position, final Direction direction) {
        super(type, position, direction);
        this.getImages().put(Direction.UP, BufferedImageLoader.loadImage("res/PacMan/Pacman_Up.png"));
        this.getImages().put(Direction.DOWN, BufferedImageLoader.loadImage("res/PacMan/Pacman_down.png"));
        this.getImages().put(Direction.LEFT, BufferedImageLoader.loadImage("res/PacMan/Pacman_left.png"));
        this.getImages().put(Direction.RIGHT, BufferedImageLoader.loadImage("res/PacMan/Pacman_right.png"));
        this.getImages().put(Direction.STOP, BufferedImageLoader.loadImage("res/PacMan/Pacman_Start.png"));
    }

    @Override
    public final void render(final Graphics g) {
        System.out.println("provo a disegnare pacman\n");
        g.drawImage(this.getImages().get(this.getDirection()), 
                    this.getPosition().getX(), this.getPosition().getY(), 25, 25, null);
    }
}
