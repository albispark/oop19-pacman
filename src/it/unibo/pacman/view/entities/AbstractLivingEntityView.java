package it.unibo.pacman.view.entities;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;
import it.unibo.pacman.view.utilities.BufferedImageLoader;

public abstract class AbstractLivingEntityView implements LivingEntityView {
    private EntityType type;
    private Pair<Integer, Integer> position;
    private Status status;
    private Dimension dimension;
    private Direction direction;
    private Map<Direction, BufferedImage> images;
    private BufferedImageLoader imageLoader;

    public AbstractLivingEntityView(final EntityType type, final Pair<Integer, Integer> position, final Direction direction) {
        this.type = type;
        this.position = position;
        this.direction = direction;
        imageLoader = new BufferedImageLoader();
        images = new HashMap<>();
    }
    @Override
    public final EntityType getType() {
        return this.type;
    }

    public final Dimension getDimension() {
        return this.dimension;
    }

//    public abstract Image getSprite();

//    @Override
//    public final void render(final Graphics g) {
//        g.drawImage(getSprite(), getPosition().getX(), getPosition().getY(), (int) getDimension().getWidth(), (int) getDimension().getHeight(), null);
//    }

    @Override
    public final void update(final Direction newDir, final Status newStatus, final Pair<Integer, Integer> newPos) {
        this.direction = newDir;
        this.status = newStatus;
        this.position = newPos;
    }

    @Override
    public final Direction getDirection() {
        return this.direction;
    }

    @Override
    public final Status getStatus() {
        return this.status;
    }

    @Override
    public final Pair<Integer, Integer> getPosition() {
        return this.position;
    }

    public final Map<Direction, BufferedImage> getImages() {
        return this.images;
    }

    public final BufferedImageLoader getLoader() {
        return this.imageLoader;
    }
}
