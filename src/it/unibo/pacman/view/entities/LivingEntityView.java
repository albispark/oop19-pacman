package it.unibo.pacman.view.entities;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.model.utilities.Status;

public interface LivingEntityView extends EntityView {
    void update(Direction newDir, Status status, Pair<Integer, Integer> newPos);
    Direction getDirection();
    Status getStatus();
    Pair<Integer, Integer> getPosition();
}
