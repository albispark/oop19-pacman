package it.unibo.pacman.view.entities;

import java.util.Set;

import it.unibo.pacman.model.utilities.Pair;

public interface UnlivingEntityView extends EntityView {
    void update(Set<Pair<Integer, Integer>> setEntity);
}
