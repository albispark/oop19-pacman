package it.unibo.pacman.view.entities;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import it.unibo.pacman.model.utilities.Direction;
import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.GhostType;
import it.unibo.pacman.model.utilities.Pair;
import it.unibo.pacman.view.utilities.BufferedImageLoader;

public class GhostView extends AbstractLivingEntityView {
    private List<BufferedImage> imagesFrightened = new LinkedList<>();

    public GhostView(final EntityType type, final Pair<Integer, Integer> position, final Direction direction, final GhostType ghost) {
        super(type, position, direction);
        this.getImages().put(Direction.UP, BufferedImageLoader.loadImage("res/Ghost/" + ghost.toString() + "_Up.png"));
        this.getImages().put(Direction.DOWN, BufferedImageLoader.loadImage("res/Ghost/" + ghost.toString() + "_Down.png"));
        this.getImages().put(Direction.LEFT, BufferedImageLoader.loadImage("res/Ghost/" + ghost.toString() + "_Left.png"));
        this.getImages().put(Direction.RIGHT, BufferedImageLoader.loadImage("res/Ghost/" + ghost.toString() + "_Right.png"));
        imagesFrightened.add(BufferedImageLoader.loadImage("res/Ghost/Frightened_Phantom.png"));
        imagesFrightened.add(BufferedImageLoader.loadImage("res/Ghost/Frightened_End_Phantom.png"));
    }

    @Override
    public final void render(final Graphics g) {
        switch (this.getStatus()) {
        case CHASED:
            g.drawImage(imagesFrightened.get(0),
                        this.getPosition().getX(), this.getPosition().getY(), 30, 30, null);
            break;
        case CHASED_END:
            g.drawImage(imagesFrightened.get(1), this.getPosition().getX(),
                        this.getPosition().getY(), 30, 30, null);
            break;
        case CHASING:
            g.drawImage(this.getImages().get(this.getDirection()),
                        this.getPosition().getX(), this.getPosition().getY(), 30, 30, null);
            break;
        default:
            break;
        }
    }
}
