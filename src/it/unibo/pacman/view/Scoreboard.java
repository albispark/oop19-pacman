package it.unibo.pacman.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public final class Scoreboard {

    private static final int DIMENSION = 600;

    private Scoreboard(final String scorePath, final String leadPath) throws IOException {
        final BufferedReader score = new BufferedReader(new FileReader(scorePath));
        final BufferedReader leaderboard = new BufferedReader(new FileReader(leadPath));

        JFrame frame = new JFrame();
        JPanel panel = new JPanel(new BorderLayout());
        JLabel label = new JLabel("Your score: " + score.readLine());
        JLabel lead = new JLabel();
        String lead2 = leaderboard.readLine();
        System.out.println(leaderboard.readLine());
        String temp;
        panel.setBackground(Color.BLACK);
        label.setFont(new Font("SANS", Font.ROMAN_BASELINE, 40));
        label.setForeground(Color.WHITE);
        panel.add(label, BorderLayout.NORTH);
        while (leaderboard.readLine() != null) {
            temp = leaderboard.readLine();
            lead2.concat(temp);
        }
        System.out.println(lead2);
        lead.setText(lead2);
        lead.setForeground(Color.WHITE);
        lead.setFont(new Font("SANS", Font.ROMAN_BASELINE, 40));
        panel.add(lead, BorderLayout.CENTER);


        frame.setContentPane(panel);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(DIMENSION, DIMENSION));
        frame.setResizable(false);
        score.close();
        leaderboard.close();
    }

    public static void main(final String[] args) throws IOException {
        Scoreboard scoreboard = new Scoreboard("res/Scoreboard/score.txt", "res/Scoreboard/leaderboard.txt");
    }
}
