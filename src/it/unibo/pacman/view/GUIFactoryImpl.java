package it.unibo.pacman.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUIFactoryImpl implements GUIFactory {
    private static final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
    private static final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
    private static final int FONT_SIZE = 30;
    private static final int POSITION = 100;

    /**
     * Default frame width.
     */
    public static final int FRAME_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2;
    /**
     * Default frame height.
     */
    public static final int FRAME_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() / 2;
    /**
     * {@inheritDoc}
     */
    @Override
    public JLabel createImageLabel(final String path) {
      //final URL imgURL = ClassLoader.getSystemResource(path);
        final ImageIcon icon = new ImageIcon(path);
        final JLabel imageLabel = new JLabel(icon);
        return imageLabel;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public JButton createMenuButton(final String text, final boolean border) {
        JButton jb = new JButton(text);
        jb.setFont(new Font("SANS", Font.TRUETYPE_FONT, FONT_SIZE));
        jb.setBorderPainted(border);
        jb.setFocusPainted(false);
        jb.setForeground(Color.BLACK);
        jb.setBackground(Color.YELLOW);
        return jb;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public JPanel createJPanel(final LayoutManager layout, final Color backgroundColor) {
        JPanel jp = new JPanel(layout);
        jp.setBackground(backgroundColor);
        return jp;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public JFrame createCentralizedJFrame(final String title, final int width, final int height) {
        JFrame frame = new JFrame(title);
        frame.setSize(width, height);
        frame.setLocation((SCREEN_WIDTH / 2) - (frame.getSize().width / 2), (SCREEN_HEIGHT / 2) - (frame.getSize().height / 2));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return frame;
    }
    /**
     *  {@inheritDoc}
     */
    @Override
    public JComboBox<String> createSelector(final List<String> items) {
        JComboBox<String> selector = new JComboBox<>();
        items.forEach(s -> selector.addItem(s));
        return selector;
    }
    @Override
    public JTextField createTextInputField(final boolean isEditable, final int columns) {
        JTextField textArea = new JTextField();
        textArea.setColumns(columns);
        textArea.setFont(new Font("SANS", Font.TRUETYPE_FONT, FONT_SIZE));
        textArea.setEditable(isEditable);
        return textArea;
    }
    @Override
    public JFrame createFrame() {
        final JFrame frame = new JFrame();
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(POSITION, POSITION);
        return frame;
    }
    @Override
    public JLabel createLabel() {
        JLabel label = new JLabel();
        label.setLayout(new BorderLayout());
        label.setFont(new Font("SANS", Font.TRUETYPE_FONT, FONT_SIZE));
        label.setBackground(Color.BLACK);
        label.setForeground(Color.WHITE);
        return label;
    }

}
