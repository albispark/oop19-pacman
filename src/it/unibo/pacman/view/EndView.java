package it.unibo.pacman.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import it.unibo.pacman.controller.game.GameControllerImpl;

public class EndView {

    private GUIFactoryImpl gf;
    private GameControllerImpl gc;
    private JFrame frame;
    private BackImagePanel panel;
    private JButton submit;
    private GridBagConstraints gbc;
    private JTextField jtf;

    public EndView() { //DA SOSTITUIRE CON GAMECONTROLLER
        gf = new GUIFactoryImpl();
//        this.gc = gc;
        loadEndView();
        frame.setVisible(true);
    }

    private void loadEndView() {
        loadFrame();
        loadPanel();
        loadScore();
        loadPlayerNameLable();
        loadSubmitButton();
        frame.setContentPane(panel);
    }

    private void loadFrame() {
        frame = gf.createFrame();
//        if (gc.hasWon()) {
//            frame.setTitle("YOU WON!");
//        } else {
//            frame.setTitle("YOU LOST!");
//        }
        frame.setSize(600, 600);
    }

    private void loadPanel() {
        panel = new BackImagePanel(new GridBagLayout(), "res/Scoreboard/PacMan_EndView.png");
        gbc = new GridBagConstraints();
    }

    private void loadScore() {
        JLabel scoreLabel = gf.createLabel();
        scoreLabel.setText("YOUR SCORE: 1000"); // da sostituire con gc.getScore
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(scoreLabel, gbc);
    }

    private void loadPlayerNameLable() {
        jtf = gf.createTextInputField(true, 10);
        jtf.setBackground(Color.BLACK);
        jtf.setForeground(Color.WHITE);
        JScrollPane jsp = new JScrollPane(jtf);
        gbc.gridx = 0;
        gbc.gridy = 1;
        panel.add(jsp, gbc);
    }

    private void loadSubmitButton() {
        submit = gf.createMenuButton("Submit", false);
        submit.addActionListener(al -> {
            String name = jtf.getText();
            // RICHIAMO CLASSE PER LEGGERE FILE
        });
        submit.setForeground(Color.WHITE);
        gbc.gridx = 0;
        gbc.gridy = 2;
        panel.add(submit, gbc);
    }

    public static void main(final String[] args) {
        EndView endView = new EndView();
    }
}
