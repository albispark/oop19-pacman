package it.unibo.pacman.view.mapEditor;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.view.GUIFactory;
import it.unibo.pacman.view.MainMenuView;

public class MapEditorController {
    //MAGARI NON STATIC FINAL. VALUTARE LA POSSIBILITA' DI CAMBIARLI IN FUTURO
    private static final int GRID_HEIGHT = 19;
    private static final int GRID_WIDTH = 17;
    private static final int TILE_WIDTH = 32;
    private static final int TILE_HEIGHT = TILE_WIDTH;
    
    //SBAGLIATO . NON PUO' ESSERE COMPOSTO DA VIEW. USARE EntityView AL POSTO DI Tile
    private Tile selectedTile;
    
    private View view;
    private GridView gridView;
    private GridModel gridModel;
    private GridMouseListener listener;
    private SaveMapController saver; //CAMPO DA ELIMINARE. DOVRà ESSERE NEL CONTROLLER E SI RICHIAMERà UN SUO METODO
    
    public MapEditorController(final MainMenuView mainMenu, final GUIFactory gFactory) {
        super();
        //SE INIZIALIZZATE QUA LA GUI RITARDA SUBITO. SPOSTARE IN UN "LOAD" (??)
        this.gridModel = new GridModel(GRID_HEIGHT, GRID_WIDTH);
        this.gridView = new GridView(GRID_HEIGHT, GRID_WIDTH, this);
        this.view = new View(mainMenu, gFactory, gridView, this);
        this.listener = new GridMouseListener(this);
        //valutare se non aggiungere un listener per ogni GridView
        this.gridView.addMouseListener(listener);
        this.saver = new SaveMapController();
        this.setSelectedTile(new Tile(EntityType.EMPTY));
    }
    
    public final void setSelectedTile(final Tile selectedTile) {
        this.selectedTile = selectedTile;
    }
    
    //o boolean se non è possibile
    public final void updatePos(final int x, final int y) {
        if (this.gridModel.isPositionModifiable(x, y)) {
            this.gridModel.setEntity(x, y, this.selectedTile.getType());
            this.gridView.updateGrid(x, y, this.selectedTile);                  //LA VIEW ANDREBBE AGGIORNATA CON LA MODEL INVECE CHE SelectedTile (??)
        }
    }
    
    public final EntityType getType(final int x, final int y) {
        return this.gridModel.getTypeByPosition(x, y);
    }
    
    public final View getView() {
        return this.view;
    }
    
    //METODO DEL SAVER DA RIGUARDARE
    public void save() {
        System.out.println("Sono entrato nel controller" + this.gridModel.getMap().toString());
        saver.saveToFile("alberto.txt", this.gridModel.getMap());
    }
    
}
