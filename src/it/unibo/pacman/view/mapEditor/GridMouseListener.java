package it.unibo.pacman.view.mapEditor;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GridMouseListener extends MouseAdapter {
    
    //PRESI DALLA "View.java" -> PASSARLE IN QUALCHE MODO
    private static final int TILE_WIDTH = 30;
    private static final int TILE_HEIGHT = TILE_WIDTH;
    
    private MapEditorController controller;
    
    public GridMouseListener(final MapEditorController controller) {
        super();
        this.controller = controller;
    }

    @Override
    public final void mousePressed(final MouseEvent e) {
        System.out.println("Pressed: " + e.getX() + ", " + e.getY());   //---------------- SYSTEM OUT DA ELIMINARE
        final int x = e.getX() / TILE_WIDTH;
        final int y = e.getY() / TILE_HEIGHT;
        System.out.println("Pressed: " + e.getX() + ", " + e.getY());   // ---------------- SYSTEM OUT DA ELIMINARE
        this.controller.updatePos(x, y);
    }
    
    @Override
    public final void mouseDragged(MouseEvent e) {
        System.out.println("Dragged: " + e.getX() + ", " + e.getY());
        this.mousePressed(e);
    }

}
