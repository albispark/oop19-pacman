package it.unibo.pacman.view.mapEditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;

public class GridModel {
    private final int rows;
    private final int columns;
    //boolean dice se è modificabile o meno (untouchable) -- CONSIDERARE DI CREARE UNA NUOVA CLASSE (NESTED NEL CASO)
    private List<List<GridEntity>> map;
    private Set<Pair<Integer, Integer>> untouchablePos;

    GridModel(final int rows, final int columns) {
        //REFUSO TRA COLONNE E RIGHE (DA RINOMINARE TUTTI CON Refactor), MA RIGUARDARE CON CALMA
        this.columns = rows;
        this.rows = columns;
        this.untouchablePos = Collections.emptySet();
        this.loadMap();
    }
    
    private void loadMap() {
        this.map = this.createEmptyMap();
        
        //mettere tutta la parte untouchable come metodo abstract (?). template method da valutare
        this.addUntouchablePortals();
        this.addUntouchableBorders();
        this.addUntouchableCenter();
    }
    
    private List<List<GridEntity>> createEmptyMap() {
        List<List<GridEntity>> tmpMap = new ArrayList<>();
        this.fillMap(tmpMap, EntityType.EMPTY);
        return tmpMap;
    }
    
    private void fillMap(final List<List<GridEntity>> mapToFill, final EntityType type) {
        for (int y = 0; y < this.columns; y++) {
            mapToFill.add(new ArrayList<GridEntity>());
            for (int x = 0; x < this.rows; x++) {
                mapToFill.get(y).add(new GridEntity(type, true));
            }
        }
    }
    
    private void addUntouchableBorders() {
        final int centerY = (this.columns - 1) / 2;
        for (int y = 1; y < this.columns - 1; y++) {
            if (y != centerY) {
                this.setUnmodifiableEntity(0, y, EntityType.WALL);
                this.setUnmodifiableEntity(this.rows - 1, y, EntityType.WALL);
            }
        }
        for (int x = 0; x < this.rows; x++) {
            this.setUnmodifiableEntity(x, 0, EntityType.WALL);
            this.setUnmodifiableEntity(x, this.columns - 1, EntityType.WALL);
        }
    }
    
    private void addUntouchablePortals() {
        final int centerY = (this.columns - 1) / 2;
        for (int xLeft = 0, xRight = this.rows - 3; xLeft < 3 && xRight < this.rows; xLeft++, xRight++) {
            this.setUnmodifiableEntity(xLeft, centerY - 1, EntityType.WALL);
            this.setUnmodifiableEntity(xLeft, centerY, EntityType.EMPTY);
            this.setUnmodifiableEntity(xLeft, centerY + 1, EntityType.WALL);
            this.setUnmodifiableEntity(xRight, centerY - 1, EntityType.WALL);
            this.setUnmodifiableEntity(xRight, centerY, EntityType.EMPTY);
            this.setUnmodifiableEntity(xRight, centerY + 1, EntityType.WALL);
        }
    }
    
    //AGGIUNGE LE ENTITA' NON MODIFICABILI DEL CENTRO
    private void addUntouchableCenter() {
        final int centerX = (this.rows - 1) / 2;
        final int centerY = (this.columns - 1) / 2;
        this.setUnmodifiableEntity(centerX, centerY + 2, EntityType.PACMAN);
        this.setUnmodifiableEntity(centerX, centerY - 1, EntityType.PINKY);
        this.setUnmodifiableEntity(centerX, centerY, EntityType.BLINKY);
        this.setUnmodifiableEntity(centerX - 1, centerY, EntityType.INKY);
        this.setUnmodifiableEntity(centerX + 1, centerY, EntityType.CLYDE);
        for (int y = centerY - 1; y <= centerY; y++) {
            this.setUnmodifiableEntity(centerX - 2, y, EntityType.WALL);
            this.setUnmodifiableEntity(centerX + 2, y, EntityType.WALL);
        }
        for (int x = centerX - 2; x <= centerX + 2; x++) {
            this.setUnmodifiableEntity(x, centerY + 1, EntityType.WALL);
        }
        for (int y = centerY - 2; y <= centerY + 2; y++) {
            for (int x = centerX - 3; x <= centerX + 3; x++) {
                if (this.isPositionModifiable(x, y)) {
                    this.setUnmodifiableEntity(x, y, EntityType.EMPTY);     //necessario per impostare EMPY come non modificabile
                }
            }
        }
    }
    
    public final boolean isPositionModifiable(final int x, final int y) {
        return this.map.get(y).get(x).isModifiable();
    }
    
    public final EntityType getTypeByPosition(final int x, final int y) {
        //System.out.println("Coordanite richiamate  y (riga) :" + y + ", x (colonna): " + x);                         //---------------------------STAMPA DA ELIMINARE
        return this.map.get(y).get(x).getType();
    }
    
    //per solo uso interno, non modificabili
    private void setUnmodifiableEntity(final int x, final int y, final EntityType type) {
        if (this.isPositionModifiable(x, y)) {
            this.map.get(y).set(x, new GridEntity(type, false));
        } 
//        else {
//            //System.out.println("UNMODIFIABLE x: " + x + " y: " + y);                                            //--------------------TO DELETE PRINTLN
//        }
    }
    
    //per uso esterno, sempre modificabili
    public final void setEntity(final int x, final int y, final EntityType type) {
        if (this.isPositionModifiable(x, y)) {
            this.map.get(y).set(x, new GridEntity(type, true));
        }
    }
    
    public final List<List<GridEntity>> getMap() {
        return this.map;
    }
    
    public static class GridEntity {
        private final EntityType type;
        private final boolean modifiable;
        
        GridEntity(final EntityType type, final boolean modifiable){
            this.type = type;
            this.modifiable = modifiable;
        }
        
        public final EntityType getType() {
            return type;
        }
        
        public final boolean isModifiable() {
            return modifiable;
        }
        
    }


    
    // -------------------- DA RIMUOVERE IL MAIN -------------------------------
//    public static void main(final String[] args) {
//        GridModel gm = new GridModel(19, 17);
//        for (int y = 0; y < 19; y++) {
//            for (int x = 0; x < 17; x++) {
//                System.out.print(gm.getTypeByPosition(x, y) + "-" + gm.isPositionModifiable(x, y) + "\t");
//            }
//            System.out.println("\n");
//        }
//    }
    
}
