package it.unibo.pacman.view.mapEditor;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.view.utilities.BufferedImageLoader;

public class Tile {
    
    //ANCHE STATIC ?!
    private final Map<EntityType, String> entityPath = new HashMap<>();
    
    //PATH DA PRENDERE DALLE VARIE "EntityView"
    //RIGUARDARE DALL SLIDE COME FUNZIONA QUESTA INIZIALIZZAZIONE. FORSE CI VA UN "static" DAVANTI
    {
    entityPath.put(EntityType.PACMAN, "res/PacMan/Pacman_Start.png");
    entityPath.put(EntityType.WALL, "res/Map/blueBlock.png");
    entityPath.put(EntityType.PILL, "");
    entityPath.put(EntityType.POWERPILL, "res/Map/yellowBlock.png");
    entityPath.put(EntityType.BLINKY, "res/Ghost/Blinky_Up.png");
    entityPath.put(EntityType.INKY, "res/Ghost/Inky_Up.png");
    entityPath.put(EntityType.CLYDE, "res/Ghost/Clyde_Up.png");
    entityPath.put(EntityType.PINKY, "res/Ghost/Pinky_Up.png");
    entityPath.put(EntityType.EMPTY, "res/Map/blackBlock2.png");
    }
    
    //private BufferedImageLoader imageLoader;
    private EntityType type;
    
    private BufferedImage image; //o Image ? 
    
    Tile(final EntityType type) {
        this.type = type;
        this.image = BufferedImageLoader.loadImage(this.getPath());
    }
    
    public final EntityType getType() {
        return type;
        
    }
    
    private String getPath() {
        return this.entityPath.get(type);
    }
    
    public final Image getImage() {
        return this.image;
    }
    
    //per JButton
    public final Icon getIcon() {
        return new ImageIcon(image);
    }
    
}
