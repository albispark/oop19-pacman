package it.unibo.pacman.view.mapEditor;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.model.utilities.Pair;

public class GridView extends JPanel {
    
    //CI SONO ANCHE NEL CONTROLLER. VEDERE DOVE METTERLE
    private static final int TILE_WIDTH = 32;
    private static final int TILE_HEIGHT = TILE_WIDTH;
    
    private List<List<GridTile>> gridMap = new ArrayList<>();
    private MapEditorController controller;
    
    public GridView(final int gridHeight, final int gridWidth, final MapEditorController controller) {
        
        super(new GridLayout(gridHeight, gridWidth));
        
        //USA STREAM! GUARDA a06.sol2
//        this.map.stream().forEach(l -> l.stream().
//                                        ./*altro*/);; //aggiungi caselle Empty
        for (int y = 0; y < gridHeight; y++) {
            gridMap.add(new ArrayList<GridTile>());
            for (int x = 0; x < gridWidth; x++) {
                //gridMap.get(y).add(new GridTile(new Tile(controller.getType(y, x) /*EntityType.EMPTY*/)));    //NON CORRETTA, MA VA -> PROBABILMENTE A CAUSA DELLA ADD NEL PANNELLO A RIGA 33
                gridMap.get(y).add(new GridTile(new Tile(controller.getType(x, y) /*EntityType.EMPTY*/)));
                this.add(gridMap.get(y).get(x));
                //System.out.print(gridMap.get(y).get(x).getTile().getType() + "\t");                              //--------------------TO DELETE PRINTLN
            }
            //System.out.println("\n");                                                        //--------------------TO DELETE PRINTLN
        }
    }
    
    public final void updateGrid(final int x, final int y, final Tile tile) {
        this.gridMap.get(y).get(x).setTile(tile);
    }

    private class GridTile extends JPanel {
        
        private static final long serialVersionUID = 8127828009105626334L;
        
        /**
         * The tile that the GridTile should show.
         */
        private Tile tile;
        
        /**
         * Construct a tile.
         * @param icon The icon of the tile.
         * @param character The character that will represent the tile when saved.
         */
        GridTile(final Tile tile) {
            this.tile = tile;
        }

        /**
         * Give the JPanel GridTile a new tile that it should show.
         * @param tile
         */
        public void setTile(final Tile tile) {
                this.tile = tile;
                this.repaint();
        }
        
        //METODO MOMENTANEO PER I TEST
        public Tile getTile() {
           return this.tile;
        }

        @Override
        public void paintComponent(final Graphics g) {
                //g.drawImage(tile.getImage(), 0, 0, null);
                g.drawImage(tile.getImage(), 0, 0, TILE_WIDTH, TILE_HEIGHT, null);
        }
    }
}
