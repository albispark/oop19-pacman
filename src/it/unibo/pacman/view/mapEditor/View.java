package it.unibo.pacman.view.mapEditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import it.unibo.pacman.model.utilities.EntityType;
import it.unibo.pacman.view.GUIFactory;
import it.unibo.pacman.view.MainMenuView;

public class View {
    private static final String EDITOR_TITLE = "MAP EDITOR";
    private static final int WIDTH = 600;
    private static final int HEIGHT = 500;
    private static final String HELP = "Add walls and powerpills to the map.\nEmpty spaces will be filled "
            + "with normal pills.\nWhen you are done, save the map by clicking SAVE and give it a name!";
    
    //-----------COSTANTI COPIA INCOLLATI DA CONSTANTS DEL MAPBUILDER -- PASSARLI DAL CONTROLLER ----------
    private static final int GRID_HEIGHT = 19;
    private static final int GRID_WIDTH = 17;
    private static final int TILE_WIDTH = 30;
    private static final int TILE_HEIGHT = TILE_WIDTH;
    //------------------------------------------------------------------------------
    
    private JFrame frame;
    
    private final GUIFactory gFactory;
    private MainMenuView mainMenu;
    private MapEditorController controller;
    private GridView grid;

    public View(final MainMenuView mainMenu, final GUIFactory gFactory, final GridView gridView, final MapEditorController controller) {
        this.mainMenu = mainMenu;
        this.gFactory = gFactory;
        this.controller = controller;
        this.grid = gridView;
        
        grid.setPreferredSize(new Dimension(GRID_WIDTH * TILE_WIDTH, GRID_HEIGHT * TILE_HEIGHT));
        grid.setBackground(Color.BLACK);
        
        frame = new JFrame(); //non va bene metodo createCentralizedJFrame della factory per via delle dimensioni prefissate

        this.load();

        frame.pack();
        frame.setResizable(false);

    }

  //DA METTERE IN UNA INTERFACCIA A PARTE
    public final void load() {
        final JPanel mainPanel = this.gFactory.createJPanel(new BorderLayout(), Color.BLACK);
        final JPanel northPalette = this.gFactory.createJPanel(new FlowLayout(), Color.BLACK);
        // TODO aggiungere tutte le immagini alla Palette
        Tile canc = new Tile(EntityType.EMPTY);
        Tile blueWall = new Tile(EntityType.WALL);
        Tile powerPill = new Tile(EntityType.POWERPILL);
        List<Tile> choices = new LinkedList<Tile>();
        choices.add(canc);
        choices.add(blueWall);
        choices.add(powerPill);
        //System.out.println("choiches: " + choices.toString());
        for (Tile t : choices) {
            JButton button = new JButton();
            button.setPreferredSize(new Dimension(TILE_WIDTH, TILE_HEIGHT));
            button.setIcon(t.getIcon());
            button.addActionListener(e -> {
                controller.setSelectedTile(t);
            });
            //button.addActionListener(controller);
            //button.setActionCommand(Character.toString(t.getCharacter()));
            northPalette.add(button);
        }
        //
        final JPanel southButtons = this.gFactory.createJPanel(new FlowLayout(), Color.BLACK);
        JButton jbSave = this.gFactory.createMenuButton("SAVE", false);
        jbSave.addActionListener(e -> {
            controller.save();
        });
        JButton jbHelp = this.gFactory.createMenuButton("HELP", false);
        jbHelp.addActionListener(e -> {
            this.showHelpDialog();
        });
        JButton jbClose = this.gFactory.createMenuButton("MENU", false);
        jbClose.addActionListener(e -> {
            this.close();
        });
        southButtons.add(jbClose);
        southButtons.add(jbHelp);
        southButtons.add(jbSave);


        mainPanel.add(northPalette, BorderLayout.NORTH);
        mainPanel.add(grid, BorderLayout.CENTER);
        mainPanel.add(southButtons, BorderLayout.SOUTH);
        this.frame.add(mainPanel);
    }
    
//    private void loadPalette(JPanel northPalette) {
//        Tile canc = new Tile(EntityType.EMPTY, "res/Map/blackBlock.png");
//        Tile blueWall = new Tile(EntityType.WALL, "res/Map/blueBlock.png");
//        Tile powerPill = new Tile(EntityType.POWERPILL, "res/Map/yellowBlock.png");
//        List<Tile> choices = new LinkedList<Tile>();
//        choices.add(canc);
//        choices.add(blueWall);
//        choices.add(powerPill);
//        //System.out.println("choiches: " + choices.toString());
//        for (Tile t : choices) {
//            JButton button = new JButton();
//            button.setPreferredSize(new Dimension(TILE_WIDTH, TILE_HEIGHT));
//            button.setIcon(t.getIcon());
//            //button.addActionListener(controller);
//            //button.setActionCommand(Character.toString(t.getCharacter()));
//            northPalette.add(button);
//        }
//    }

  //DA METTERE IN UNA INTERFACCIA A PARTE
    public final void show() {
        this.mainMenu.close();
        this.frame.setVisible(true);
        this.showHelpDialog();
    }

    private void showHelpDialog() {
        JOptionPane.showMessageDialog(frame, HELP);
    }

  //DA METTERE IN UNA INTERFACCIA A PARTE
    public final void close() {
        this.frame.setVisible(false);
        this.mainMenu.show();
    }

}