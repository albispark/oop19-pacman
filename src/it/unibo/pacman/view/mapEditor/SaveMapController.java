package it.unibo.pacman.view.mapEditor;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.unibo.pacman.view.mapEditor.GridModel.GridEntity;

public class SaveMapController {
    
    private static final int GRID_HEIGHT = 19;
    private static final int GRID_WIDTH = 17;
    
    //percorso senza il nome finale della mappa che verrà aggiunto nella save
    //private String filePath =  "res" + System.getProperty("file.separator") + "Map" + System.getProperty("file.separator");
    private String filePath = "C:\\Users\\Spark\\Desktop\\JavaProject Stuff\\1. OLD REAL Project\\oop19-pacman-old\\res\\Map\\";
    
    public SaveMapController() {
        // TODO prende la lista delle mappe esistenti o come argomento o leggendola da file
    }
    
    /**
     * 
     * @param mapName
     * @param mapEntityPosition
     */
    public void saveToFile(final String mapName, final List<List<GridEntity>> mapEntityPosition) {
        this.filePath = this.filePath + mapName;
        System.out.println("Sono entrato: " + this.filePath);
        try {
            
            FileOutputStream fos = new FileOutputStream(this.filePath);
            
            DataOutputStream dos = new DataOutputStream(fos);
            
            dos.writeInt(GRID_WIDTH); //colonne
            dos.writeInt(GRID_HEIGHT); //righe
            
            for (int y = 0; y < this.GRID_HEIGHT; y++) {
                for (int x = 0; x < this.GRID_WIDTH; x++) {
                    dos.writeInt(mapEntityPosition.get(y).get(x).getType().getValue());
                }
            }
            
            
        } catch (IOException  e) {
            System.out.println("IOException : " + e);
            e.printStackTrace(); //autoGenerato
        }
    }
}
