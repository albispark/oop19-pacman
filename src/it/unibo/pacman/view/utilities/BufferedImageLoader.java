package it.unibo.pacman.view.utilities;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class BufferedImageLoader { 

    private static BufferedImage image;

    public static final BufferedImage loadImage(final String path) {
        try {
            //image = ImageIO.read(getClass().getResource(path));
            image = ImageIO.read(new File(path)); //metodo preso dal tutorial Oracle/Javadoc
            System.out.println("OK!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

}
