package it.unibo.pacman.view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import it.unibo.pacman.controller.game.GameController;
import it.unibo.pacman.controller.game.GameControllerImpl;
import it.unibo.pacman.view.mapEditor.MapEditorController;
import it.unibo.pacman.view.mapEditor.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

/**
 * It is the main menu of the application.
 *
 */
public class MainMenuView {
    //MEGLIO METTERLI DIRETTAMENTE A DOUBLE E VEDERE POI COME CENTRARE LE SCHERMO
//    private static final int WIDTH = 600;
//    private static final int HEIGHT = 500;
    private static final String TITLE_PATH = "res/pacmanSimpi.jpg";
    private final GUIFactory gFactory;
    private MapEditorController mapEditor;
    private GameController gc;
    private final JFrame frame;
    /**
     * Creates the menu.
     * 
     * @param gameTitle it is the name of the game.
     */
    public MainMenuView(final String gameTitle) {
        gFactory = new GUIFactoryImpl();
        this.gc = new GameControllerImpl(this);
        mapEditor = new MapEditorController(this, gFactory);
        frame = gFactory.createCentralizedJFrame(gameTitle, GUIFactoryImpl.FRAME_WIDTH, GUIFactoryImpl.FRAME_HEIGHT);
        frame.setResizable(false);
    }
    /**
     * It load the menu.
     */

  //DA METTERE IN UNA INTERFACCIA A PARTE
    public void load() {
        this.loadTitle();
        this.loadOptionsList();
        frame.pack();
        this.show();
    }

    private void loadTitle() {
        final JPanel northPanel = gFactory.createJPanel(new FlowLayout(), Color.BLACK);
        final JLabel titleLable = gFactory.createImageLabel(TITLE_PATH);       //NON VA . . . . ?? ? ? ? ? ?
        northPanel.setPreferredSize(new Dimension(frame.getWidth(), frame.getHeight() / 4));
        northPanel.add(titleLable);
        frame.add(northPanel, BorderLayout.NORTH);
    }

    private void loadOptionsList() {
        final JPanel optionsList = gFactory.createJPanel(new GridBagLayout(), Color.YELLOW);
        final GridBagConstraints costr = new GridBagConstraints();
        costr.gridy = 0;
        costr.insets = new Insets(20, 20, 20, 20);
        costr.fill = GridBagConstraints.HORIZONTAL;
        //da aggiungere roba
        final JButton jbStart = gFactory.createMenuButton("START GAME", false);
        jbStart.addActionListener(e -> {
            this.gc.startGame("standard.txt");
        });
        optionsList.add(jbStart, costr);
        costr.gridy++;
        final JButton jbMapEditor = gFactory.createMenuButton("CREATE MAP", false);
        jbMapEditor.addActionListener(e -> {
            mapEditor.getView().show();
        });
        optionsList.add(jbMapEditor, costr);
        costr.gridy++;
        optionsList.add(gFactory.createMenuButton("LEADERBOARD", false), costr);
        costr.gridy++;
        optionsList.add(gFactory.createMenuButton("QUIT", false), costr);
        frame.add(optionsList, BorderLayout.CENTER);
    }

    //DA METTERE IN UNA INTERFACCIA A PARTE
    public final void show() {
        this.frame.setVisible(true);
    }

    //DA METTERE IN UNA INTERFACCIA A PARTE
    public final void close() {
        this.frame.setVisible(false);
    }
}
