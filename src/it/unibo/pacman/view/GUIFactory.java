package it.unibo.pacman.view;

import java.awt.Color;
import java.awt.LayoutManager;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
/**
 * 
 * GUI factory interface.
 *
 */
public interface GUIFactory {
    /**
     * Creates a JLabel with the image given by the path.
     * @param path of the image
     * @return a JLabel
     */
    JLabel createImageLabel(String path);
    /**
     * Creates a JButton for a menu.
     * @param text of the JButton
     * @param border visibility
     * @return a JButton
     */
    JButton createMenuButton(String text, boolean border);
    /**
     * Creates a JPanel.
     * @param layout of the JPanel
     * @param backgroundColor the color of the background of the JPanel
     * @return a JPanel
     */
    JPanel createJPanel(LayoutManager layout, Color backgroundColor);
    /**
     * Creates a centralized JFrame.
     * @param title of the JFrame
     * @param width of the JFrame
     * @param height of the JFrame
     * @return a centralized JFrame
     */
    JFrame createCentralizedJFrame(String title, int width, int height);
    /**
     * Creates a JComboBox.
     * @param items of the drop down menu
     * @return a JComboBox
     */
    JComboBox<String> createSelector(List<String> items);
    /**
     * Creates a text input field.
     * @param isEditable decides if the text field is editable or not
     * @param columns number of the text field
     * @return a JTextField
     */
    JTextField createTextInputField(boolean isEditable, int columns);

    JFrame createFrame();
    
    JLabel createLabel();
}
