package it.unibo.pacman.view;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import it.unibo.pacman.controller.game.KeyInput;
import it.unibo.pacman.view.entities.EntityView;

public final class SinglePlayerView {
    private int rows;
    private int columns;
    private static final int SCALE_FACTOR = 30;
    private Canvas canvas = new Canvas();
    private BufferStrategy bs;
    private JPanel panel;
    private JFrame frame;

    public SinglePlayerView(final KeyInput input, final int rows, final int columns) throws IOException  {

        System.out.println("rows: " + rows + "columns: " + columns);
        this.columns = columns;
        this.rows = rows;
        this.frame = new JFrame();
        this.panel = new JPanel(new BorderLayout());
        this.panel.setVisible(true);
        this.frame.setVisible(true);
        this.frame.add(panel);
        this.canvas.setSize(520, 600);
        this.panel.add(canvas, BorderLayout.CENTER);
        canvas.addKeyListener(input);
        bs = this.canvas.getBufferStrategy();
        if (bs == null) {
          canvas.createBufferStrategy(2);
          bs = canvas.getBufferStrategy();
        }
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
    }

    public Graphics getGraphics() {
      return this.bs.getDrawGraphics();
    }

    public void render(final Set<EntityView> setView) {
        setView.stream().forEach(v -> v.render(this.getGraphics()));
    }

    public JFrame getFrame() {
        return this.frame;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public BufferStrategy getBs() {
        return bs;
    }
}
